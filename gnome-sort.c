#include <stdio.h>

#define SWAP(x, y, T) do { T SWAP = x; x = y; y = SWAP; } while (0)

void gnome_sort(int array[], int high) {
	int i = 0;
	while (i < high) {
		if (array[i] > array[i+1]) {
			SWAP(array[i], array[i+1], int);
			if (i > 0) i--;
		}
		else i++;
	}
}

void print_array(int array[], int high) {
	for (int i = 0; i <= high; i++)
		printf("%d ", array[i]);
}

void main() {
	int array[] = {1, 3, 6, 2, 4, 73, 23, 61, 32, 64, 11, 32, 3, 2};
	gnome_sort(array, 13);
	print_array(array, 13);
}
