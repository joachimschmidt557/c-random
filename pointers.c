#include<stdio.h>

int
main() {
	int x = 13;
	int* p = &x;

	printf("x: %d\n", x);
	printf("p: %x\n", p);
	printf("*p: %d\n", *p);

	const char* str = "Pointers";
	for (const char* p = str; *p != '\0'; p++) {
		printf("%c", *p);
	}
	printf("\n");
}
