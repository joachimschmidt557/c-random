#ifndef RBTREE_H
#define RBTREE_H

typedef enum {
	Red,
	Black,
} RBColor;

typedef struct RBTreeNode {
	void* data;
	struct RBTreeNode* left;
	struct RBTreeNode* right;
	RBColor color;
} RBTreeNode;

#endif
