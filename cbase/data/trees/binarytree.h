#ifndef BINARYTREE_H
#define BINARYTREE_H

typedef struct BinaryTreeNode {
	void* data;
	struct BinaryTreeNode* left;
	struct BinaryTreeNode* right;
} BinaryTreeNode;

BinaryTreeNode*
new_binarytree();

#endif
