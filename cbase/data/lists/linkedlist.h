#ifndef LINKEDLIST_H
#define LINKEDLIST_H

typedef struct LinkedListNode {
	void* data;
	struct LinkedListNode* prev;
	struct LinkedListNode* next;
} LinkedListNode;

typedef struct LinkedList {
	LinkedListNode* nil;
} LinkedList;

LinkedList*
new_linkedlist();

LinkedListNode*
first(LinkedList* l);

LinkedListNode*
last(LinkedList* l);

void
append(LinkedList* l, void* x);

void
insert(LinkedList* l, void* x);

void
insert_at(LinkedList* l, int i, void* x);

LinkedListNode*
search(LinkedList* l, void* x);

void
delete_item(LinkedListNode* x);

void
delete_linkedlist(LinkedList* l);

int
count_linkedlist(LinkedList* l);

#endif
