#include<stdio.h>
#include<assert.h>

#include "linkedlist.h"

void
test_empty() {
	LinkedList* l = new_linkedlist();
	assert(count_linkedlist(l) == 0);
	delete_linkedlist(l);
}

void
test_single() {
	LinkedList* l = new_linkedlist();

	int x = 42;
	int* p = &x;

	append(l, p);
	assert(count_linkedlist(l) == 1);
	assert(first(l)->data == p);
	assert(last(l)->data == p);

	delete_item(first(l));
	assert(count_linkedlist(l) == 0);

	delete_linkedlist(l);
}

int
main() {
	test_empty();
	test_single();
}
